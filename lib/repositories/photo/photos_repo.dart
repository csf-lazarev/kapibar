import 'package:kapibar/models/dto/feed_dto.dart';

abstract class IPhotoRepo {
  Future<FeedDTO> load(int pageFrom, int perPage);
}
