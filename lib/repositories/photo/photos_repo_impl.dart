import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:kapibar/exceptions/photo_page_load_exception.dart';
import 'package:kapibar/models/dto/feed_dto.dart';
import 'package:kapibar/repositories/photo/photos_repo.dart';

import '../../config/constants.dart';

class PhotoRepo extends IPhotoRepo {
  static const String method = 'flickr.photos.search';
  static const String format = 'json';
  static const String text = 'capybara';
  static const String sort = 'relevance';

  static const String restApiPath = '/services/rest';

  final http.Client httpClient;

  PhotoRepo({required this.httpClient});

  @override
  Future<FeedDTO> load(int pageFrom, int perPage) {
    return httpClient
        .get(
      Uri.https(
        flicrHostURL,
        restApiPath,
        <String, String>{
          'method': method,
          'api_key': flickrApiKey,
          'format': format,
          'text': text,
          'sort': sort,
          'page': '$pageFrom',
          'per_page': '$perPage'
        },
      ),
    )
        .then((response) {
      String body = utf8.decode(response.bodyBytes);
      if (!body.startsWith(flicrResponseStart)) {
        throw PhotoPageLoadException("Not flicr response");
      }
      return FeedDTO.fromJson(
          jsonDecode(body.substring(14, body.length - 1))["photos"]);
    });
  }
}
