import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;
import 'package:kapibar/repositories/photo/photos_repo_impl.dart';
import 'package:kapibar/screens/feed/feed.dart';
import 'package:kapibar/services/photo_like_service.dart';
import 'package:kapibar/services/photo_service.dart';

import 'bloc/feed/feed_bloc.dart';
import 'config/constants.dart';

class KapiBarApp extends MaterialApp {
  KapiBarApp({super.key})
      : super(
            home: Scaffold(
              appBar: AppBar(
                title: const Text(appTitle),
                backgroundColor: Colors.teal,
              ),
              body: BlocProvider(
                create: (_) => FeedBloc(
                    PhotoService(PhotoRepo(httpClient: http.Client())),
                    PhotoLikeService())
                  ..add(FeedRequested()),
                child: const FeedWidget(),
              ),
        ));
}
