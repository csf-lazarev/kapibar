import 'package:flutter/material.dart';

class LikeCardButton extends StatelessWidget {
  final bool Function() isLiked;
  final void Function() onPressed;
  final double? buttonSize;

  const LikeCardButton(
      {super.key,
      required this.isLiked,
      required this.onPressed,
      this.buttonSize = 30});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onPressed();
      },
      child: isLiked()
          ? Icon(
              Icons.favorite,
              size: buttonSize,
              color: Colors.red,
            )
          : Icon(
              Icons.favorite_border,
              size: buttonSize,
            ),
    );
  }
}
