import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kapibar/bloc/feed/feed_bloc.dart';

import '../../ui/widgets/bottom_loader.dart';
import 'widgets/photo_card.dart';

class FeedWidget extends StatefulWidget {
  const FeedWidget({super.key});

  @override
  State<FeedWidget> createState() => _FeedWidgetState();
}

class _FeedWidgetState extends State<FeedWidget> {
  final _scrollController = ScrollController();
  static const String noPosts = 'no posts';

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onScroll);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FeedBloc, FeedState>(
      builder: (context, state) {
        switch (state.runtimeType) {
          case FeedFailure:
            return const Center(child: Text('failed to fetch posts'));
          case FeedSuccess:
            state = state as FeedSuccess;
            var photos = state.feed.photos ?? List.empty();
            if (state.feed.photos!.isEmpty) {
              return const Center(child: Text(noPosts));
            }
            return ListView.builder(
              itemBuilder: (BuildContext context, int index) {
                return index >= photos.length
                    ? const BottomLoader()
                    : PhotoCard(view: photos[index]);
              },
              itemCount:
                  state.hasReachedMax ? photos.length : photos.length + 1,
              controller: _scrollController,
            );
          case FeedInitial:
            return const Center(child: CircularProgressIndicator());
        }
        return const Center(child: Text(noPosts));
      },
    );
  }

  @override
  void dispose() {
    _scrollController
      ..removeListener(_onScroll)
      ..dispose();
    super.dispose();
  }

  void _onScroll() {
    if (_scrollController.position.userScrollDirection ==
            ScrollDirection.reverse &&
        _scrollController.position.atEdge &&
        _isBottom) {
      context.read<FeedBloc>().add(FeedRequested());
    }
  }

  bool get _isBottom {
    if (!_scrollController.hasClients) return false;
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.offset;
    return currentScroll >= (maxScroll * 0.9);
  }
}
