import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jumping_dot/jumping_dot.dart';
import 'package:kapibar/bloc/full_screen/full_screen_bloc.dart';
import 'package:kapibar/models/view/card_view.dart';
import 'package:kapibar/ui/widgets/like_card_button.dart';

import '../../../bloc/feed/feed_bloc.dart';
import '../../../services/photo_like_service.dart';
import '../../photo_card_fullscreen/fullscreen_card.dart';

class PhotoCard extends StatelessWidget {
  const PhotoCard({required this.view, super.key});

  final CardView view;

  @override
  Widget build(BuildContext context) {
    return Material(
        color: Colors.white,
        child: Container(
          margin: const EdgeInsets.fromLTRB(8, 10, 8, 0),
          padding: const EdgeInsets.fromLTRB(5, 10, 5, 10),
          decoration: BoxDecoration(
            color: Colors.white70,
            border: Border.all(color: Colors.grey.shade400),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.4),
                spreadRadius: 3,
                blurRadius: 7,
                offset: const Offset(0, 0), // changes position of shadow
              ),
            ],
          ),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                child: Center(
                  child: Text(
                    view.title!,
                    textAlign: TextAlign.start,
                    style: const TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.w400,
                      color: Colors.black,
                    ),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () => _openFullScreen(context),
                child: CachedNetworkImage(
                  placeholder: (context, url) => JumpingDots(
                    color: Colors.teal,
                    radius: 10,
                    numberOfDots: 3,
                    animationDuration: const Duration(milliseconds: 200),
                  ),
                  imageUrl: view.link!,
                  errorWidget: (context, url, error) => const Icon(Icons.error),
                ),
              ),
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(10, 10, 0, 0),
                    child: LikeCardButton(
                      isLiked: () => view.isFavorite,
                      onPressed: () =>
                          context.read<FeedBloc>().add(FeedPostLiked(view)),
                      buttonSize: 35,
                    ),
                  )
                ],
              )
            ],
          ),
        ));
  }

  void _openFullScreen(BuildContext context) {
    var feed = (context.read<FeedBloc>().state as FeedSuccess).feed;
    Navigator.of(context)
        .push<dynamic>(MaterialPageRoute<dynamic>(
            builder: (BuildContext context) => BlocProvider<FullScreenBloc>(
                  lazy: false,
                  create: (_) => FullScreenBloc(PhotoLikeService())
                    ..add(FullScreenEntered(view, feed)),
                  child: const FullScreenCardWidget(),
                )))
        .then((value) => context.read<FeedBloc>().add(FeedUpdate(feed)));
  }
}
