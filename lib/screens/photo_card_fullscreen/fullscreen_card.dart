import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kapibar/bloc/full_screen/full_screen_bloc.dart';
import 'package:photo_view/photo_view.dart';

import '../../ui/widgets/like_card_button.dart';

class FullScreenCardWidget extends StatefulWidget {
  const FullScreenCardWidget({super.key});

  @override
  State<StatefulWidget> createState() => _FullScreenCardWidgetState();
}

class _FullScreenCardWidgetState extends State<FullScreenCardWidget> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FullScreenBloc, FullScreenState>(
      builder: (context, state) {
        switch (state.runtimeType) {
          case FullScreenSuccess:
            var view = (state as FullScreenSuccess).photoView;
            return Dismissible(
                key: UniqueKey(),
                direction: DismissDirection.vertical,
                onDismissed: (_) => Navigator.of(context).pop(),
                resizeDuration: const Duration(microseconds: 100),
                child: Scaffold(
                  backgroundColor: Colors.black,
                  appBar: AppBar(
                    leading: const BackButton(),
                    title: Text(view.title!),
                    backgroundColor: Colors.black,
                  ),
                  body: Center(
                    child: PhotoView(
                      imageProvider: CachedNetworkImageProvider(view.link!),
                    ),
                  ),
                  floatingActionButton: FloatingActionButton(
                    onPressed: () {},
                    child: Center(
                        child: LikeCardButton(
                      isLiked: () => view.isFavorite,
                      onPressed: () => context
                          .read<FullScreenBloc>()
                          .add(FullScreenLiked(view)),
                    )),
                  ),
                  floatingActionButtonLocation:
                      FloatingActionButtonLocation.endFloat,
                ));
        }
        return const Icon(Icons.error);
      },
    );
  }
}
