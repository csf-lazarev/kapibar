class PhotoDTO {
  final String? _id;
  final String? _owner;
  final String? _secret;
  final String? _server;
  final String? _title;

  PhotoDTO(this._id, this._owner, this._secret, this._server, this._title);

  PhotoDTO.fromJson(Map<String, dynamic> json)
      : _id = json['id'],
        _owner = json["owner"],
        _secret = json["secret"],
        _server = json["server"],
        _title = json["title"];

  String? get title => _title;

  String? get server => _server;

  String? get secret => _secret;

  String? get owner => _owner;

  String? get id => _id;
}
