import 'package:kapibar/models/dto/photo_dto.dart';

class FeedDTO {
  int? _lastPageIndex;
  int? _perPage;
  int? _totalPages;
  List<PhotoDTO>? _photos;

  FeedDTO(this._lastPageIndex, this._perPage, this._totalPages, this._photos);

  FeedDTO.fromJson(Map<String, dynamic> json) {
    _lastPageIndex = json['page'];
    _perPage = json['perpage'];
    _totalPages = json['total'];
    if (json['photo'] != null) {
      _photos = <PhotoDTO>[];
      json['photo'].forEach((v) => _photos!.add(PhotoDTO.fromJson(v)));
    }
  }

  List<PhotoDTO>? get photo => _photos;

  int? get total => _totalPages;

  int? get perPage => _perPage;

  int? get page => _lastPageIndex;
}
