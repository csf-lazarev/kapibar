import 'package:kapibar/models/view/feed_view.dart';

import 'card_view.dart';

class LikedFeed {
  final FeedView feed;
  final CardView likedPhoto;

  LikedFeed(this.feed, this.likedPhoto);
}
