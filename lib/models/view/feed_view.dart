import 'package:kapibar/models/view/card_view.dart';

class FeedView {
  final int? _lastPageIndex;
  final int? _perPage;
  final int? _totalPages;
  List<CardView>? _photos;

  FeedView(this._lastPageIndex, this._perPage, this._totalPages, this._photos);

  List<CardView>? get photos => _photos;

  int? get totalPages => _totalPages;

  int? get perPage => _perPage;

  int? get lastPageIndex => _lastPageIndex;

  void concatWith(FeedView page) {
    List<CardView> list = List.from(page._photos as Iterable);
    list.addAll(_photos ?? List.empty());
    _photos = list
        .asMap()
        .entries
        .map((e) => CardView(
            e.value.id, e.value.title, e.value.link, e.value.isFavorite, e.key))
        .toList();
  }
}
