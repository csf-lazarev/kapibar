class CardView {
  final String? _id;
  final String? _title;
  final String? _link;
  final bool _isFavorite;
  final int _index;

  CardView(this._id, this._title, this._link, this._isFavorite, this._index);

  bool get isFavorite => _isFavorite;

  String? get link => _link;

  String? get title => _title;

  int get index => _index;

  String? get id => _id;
}
