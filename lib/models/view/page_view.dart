import 'card_view.dart';

class PageView {
  final int index;
  final List<CardView> cards;

  PageView(this.index, this.cards);
}
