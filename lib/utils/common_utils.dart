import '../config/constants.dart';
import '../models/dto/photo_dto.dart';
import '../models/view/card_view.dart';

String extractFromFlicrResponse(String origin) {
  return origin.substring(14, origin.length - 1);
}

String createImageUrl(String? server, String? id, String? secret) {
  return "$imageServerURL/$server/${id}_$secret.jpg";
}

CardView from(PhotoDTO photo, int index) {
  return CardView(photo.id, photo.title,
      createImageUrl(photo.server, photo.id, photo.secret), false, index);
}
