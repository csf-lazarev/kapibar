part of 'full_screen_bloc.dart';

abstract class FullScreenEvent extends Equatable {
  const FullScreenEvent();
}

final class FullScreenEntered extends FullScreenEvent {
  final CardView view;
  final FeedView feedView;

  const FullScreenEntered(this.view, this.feedView);

  @override
  List<Object?> get props => [view];
}

final class FullScreenLiked extends FullScreenEvent {
  final CardView likedPhoto;

  const FullScreenLiked(this.likedPhoto);

  @override
  List<Object> get props => [likedPhoto];
}
