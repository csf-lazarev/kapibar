part of 'full_screen_bloc.dart';

abstract class FullScreenState extends Equatable {
  const FullScreenState();
}

class FullScreenInitial extends FullScreenState {
  @override
  List<Object> get props => [];
}

class FullScreenSuccess extends FullScreenState {
  final CardView photoView;
  final FeedView feedView;

  const FullScreenSuccess(this.photoView, this.feedView);

  @override
  List<Object> get props => [photoView, feedView];
}
