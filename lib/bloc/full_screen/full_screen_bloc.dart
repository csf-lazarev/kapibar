import 'dart:async';
import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kapibar/models/view/card_view.dart';
import 'package:kapibar/models/view/feed_view.dart';
import 'package:kapibar/services/photo_like_service.dart';

import '../../models/view/liked_feed.dart';

part 'full_screen_event.dart';
part 'full_screen_state.dart';

class FullScreenBloc extends Bloc<FullScreenEvent, FullScreenState> {
  final PhotoLikeService photoLikeService;

  FullScreenBloc(this.photoLikeService) : super(FullScreenInitial()) {
    on<FullScreenEntered>(_onFullScreenEntered);
    on<FullScreenLiked>(_onFullScreenLiked);
  }

  Future<void> _onFullScreenEntered(
    FullScreenEntered event,
    Emitter<FullScreenState> emit,
  ) async {
    log("Emit fullscreen");
    emit(FullScreenSuccess(event.view, event.feedView));
  }

  Future<void> _onFullScreenLiked(
    FullScreenLiked event,
    Emitter<FullScreenState> emit,
  ) async {
    if (state.runtimeType != FullScreenSuccess) {
      return;
    }
    var successState = state as FullScreenSuccess;
    var updatedFeed = await photoLikeService
        .performLikeAction(LikedFeed(successState.feedView, event.likedPhoto));
    log("Like action on photo ${event.likedPhoto.index} preformed");
    emit(FullScreenSuccess(
        updatedFeed.photos![event.likedPhoto.index], updatedFeed));
  }
}
