part of 'feed_bloc.dart';

abstract class FeedEvent extends Equatable {
  const FeedEvent();

  @override
  List<Object> get props => [];
}

final class FeedRequested extends FeedEvent {}

final class FeedPostLiked extends FeedEvent {
  final CardView likedPhoto;

  const FeedPostLiked(this.likedPhoto);

  @override
  List<Object> get props => [likedPhoto];
}

final class FeedUpdate extends FeedEvent {
  final FeedView feedView;

  const FeedUpdate(this.feedView);

  @override
  List<Object> get props => [feedView];
}
