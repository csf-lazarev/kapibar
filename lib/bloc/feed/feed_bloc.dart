import 'dart:async';
import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kapibar/models/view/card_view.dart';
import 'package:kapibar/models/view/feed_view.dart';
import 'package:kapibar/models/view/liked_feed.dart';

import '../../config/constants.dart';
import '../../services/photo_like_service.dart';
import '../../services/photo_service.dart';

part 'feed_event.dart';
part 'feed_state.dart';

class FeedBloc extends Bloc<FeedEvent, FeedState> {
  final PhotoService photoService;
  final PhotoLikeService photoLikeService;

  FeedBloc(this.photoService, this.photoLikeService)
      : super(const FeedInitial(false)) {
    on<FeedRequested>(_onFeedFetched);
    on<FeedPostLiked>(_onFeedLiked);
    on<FeedUpdate>(_onFeedUpdate);
  }

  Future<void> _onFeedFetched(
    FeedRequested event,
    Emitter<FeedState> emit,
  ) async {
    if (state.hasReachedMax) return;
    try {
      if (state.runtimeType == FeedInitial) {
        var photoPage = await photoService.retrievePage(1, photosPerPage);
        log("Show page ${photoPage.lastPageIndex}...");
        emit(FeedSuccess(
            false, photoPage, DateTime.now().microsecondsSinceEpoch));
      } else if (state.runtimeType == FeedSuccess) {
        var retrievePage =
            await photoService.retrievePage(state.pageIndex + 1, photosPerPage);
        retrievePage.concatWith((state as FeedSuccess).feed);
        log("Show page ${retrievePage.lastPageIndex}...");
        emit(FeedSuccess(retrievePage.photos!.isEmpty, retrievePage,
            DateTime.now().microsecondsSinceEpoch));
      }
    } catch (e, s) {
      emit(const FeedFailure(false));
    }
  }

  Future<void> _onFeedLiked(
    FeedPostLiked event,
    Emitter<FeedState> emit,
  ) async {
    if (state.runtimeType != FeedSuccess) {
      return;
    }
    var successState = state as FeedSuccess;
    var updatedFeed = await photoLikeService
        .performLikeAction(LikedFeed(successState.feed, event.likedPhoto));
    log("Like action on photo ${event.likedPhoto.index} preformed");
    emit(FeedSuccess(state.hasReachedMax, updatedFeed,
        DateTime.now().microsecondsSinceEpoch));
  }

  Future<void> _onFeedUpdate(FeedUpdate event, Emitter<FeedState> emit) async {
    emit(FeedSuccess(state.hasReachedMax, event.feedView,
        DateTime.now().microsecondsSinceEpoch));
  }
}
