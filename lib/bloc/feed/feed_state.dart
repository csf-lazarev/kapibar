part of 'feed_bloc.dart';

class FeedState extends Equatable {
  final bool hasReachedMax;

  const FeedState(this.hasReachedMax);

  @override
  List<Object?> get props => [hasReachedMax];

  int get pageIndex => 0;
}

class FeedInitial extends FeedState {
  const FeedInitial(super.hasReachedMax);
}

class FeedFailure extends FeedState {
  const FeedFailure(super.hasReachedMax);
}

class FeedSuccess extends FeedState {
  final FeedView feed;
  final int lastModified;

  const FeedSuccess(super.hasReachedMax, this.feed, this.lastModified);

  @override
  List<Object?> get props => [feed, hasReachedMax, lastModified];

  @override
  int get pageIndex => feed.lastPageIndex ?? 0;
}
