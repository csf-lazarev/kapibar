import 'package:kapibar/models/view/feed_view.dart';
import 'package:kapibar/repositories/photo/photos_repo.dart';

import '../utils/common_utils.dart';

class PhotoService {
  final IPhotoRepo photoRepo;

  PhotoService(this.photoRepo);

  Future<FeedView> retrievePage(int startFromIndex, int maxPhotos) {
    return photoRepo.load(startFromIndex, maxPhotos).then((feedDTO) => FeedView(
        feedDTO.page,
        feedDTO.perPage,
        feedDTO.total,
        feedDTO.photo
            ?.asMap()
            .entries
            .map((entry) => from(entry.value, entry.key))
            .toList()));
  }
}
