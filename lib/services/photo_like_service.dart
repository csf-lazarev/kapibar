import 'package:kapibar/models/view/card_view.dart';
import 'package:kapibar/models/view/feed_view.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/view/liked_feed.dart';

class PhotoLikeService {
  Future<FeedView> performLikeAction(LikedFeed likedFeed) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    var likedPhoto = likedFeed.likedPhoto;
    prefs.setBool(likedPhoto.id!, !likedPhoto.isFavorite);
    var photo = likedFeed.feed.photos?[likedPhoto.index];
    likedFeed.feed.photos?[photo!.index] = CardView(
        photo.id, photo.title, photo.link, !photo.isFavorite, photo.index);
    return likedFeed.feed;
  }
}
