class PhotoPageLoadException implements Exception {
  final String message;

  PhotoPageLoadException(this.message);

  @override
  String toString() => 'PhotoPageLoadException: $message';
}
